require("dotenv").config();
const express = require("express");
const app = express();
const PORT = process.env.PORT;
const cors = require("cors");
const swaggerUI = require("swagger-ui-express");

const { auth, logistic } = require("./route");
const { jwtAuth } = require("./middleware");
const docs = require("./docs");

const server = app.listen(PORT, () => console.log(`App Listen Port ${PORT}`));

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use(cors());
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(docs));

// Endpoint Auth

app.post("/register", auth.register);
app.post("/login", auth.login);
app.get("/getperson", jwtAuth, auth.getdata);

// Endpoint Logistic

app.get("/getkurir", jwtAuth, logistic.getdatakurir);
app.get("/kurir", logistic.kurirdestinasi);

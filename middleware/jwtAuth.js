const jwt = require("jsonwebtoken");

const config = process.env;

const verifyToken = (req, res, next) => {
  const token =
    req.body.token ||
    req.query.token ||
    req.headers["x-access-token"] ||
    req.headers.authorization;

  if (!token) {
    return res.status(403).send({
      code: 403,
      message: "A token is required for authentication",
      data: {},
      err: "Forbidden",
    });
  }
  try {
    const decodedToken = jwt.verify(token, config.SECRET);

    req.user = decodedToken;
  } catch (err) {
    return res.status(401).send({
      code: 401,
      message: "Invalid token",
      data: {},
      err: "Unauthorized",
    });
  }
  return next();
};

module.exports = verifyToken;

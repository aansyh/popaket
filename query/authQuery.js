const db = require("../database");

const registerQuery = async (body) => {
  return new Promise((resolve, reject) => {
    let { username, msisdn, password } = body;
    let checkSql = `SELECT * FROM person WHERE username ilike '${username}' and msisdn = '${msisdn}'`;
    let sql = `INSERT INTO person (username, msisdn, password) values ('${username}', ${msisdn}, '${password}') RETURNING ID, USERNAME`;

    db.query(checkSql).then((res) => {
      if (res.rowCount >= 1) {
        return reject(res);
      } else {
        db.query(sql)
          .then((res) => {
            if (res.rows.length >= 1) {
              return resolve(res.rows);
            }
          })
          .catch((err) => {
            return reject(err);
          });
      }
    });
  })
    .then((result) => {
      return result;
    })
    .catch((err) => {
      return err;
    });
};

const loginQuery = async (body) => {
  return new Promise((resolve, reject) => {
    let { msisdn, password } = body;

    let checkSql = `SELECT * FROM person WHERE msisdn = '${msisdn}' and password ilike '${password}'`;

    db.query(checkSql)
      .then((res) => {
        if (res.rows.length >= 1) {
          return resolve(res);
        } else {
          return resolve(res);
        }
      })
      .catch((err) => {
        return reject(err);
      });
  })
    .then((result) => {
      return result;
    })
    .catch((err) => {
      return err;
    });
};

module.exports = { registerQuery, loginQuery };

const authQuery = require("./authQuery.js");
const logisticQuery = require("./logisticQuery");

module.exports = { authQuery, logisticQuery };

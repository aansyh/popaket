const db = require("../database");

const getkurirQuery = async () => {
  return new Promise((resolve, reject) => {
    let sql = `SELECT * FROM logistic`;

    db.query(sql)
      .then((res) => {
        console.log(res);
        return resolve(res.rows);
      })
      .catch((err) => {
        return reject(err);
      });
  })
    .then((result) => {
      return result;
    })
    .catch((err) => {
      return err;
    });
};

const kurirdestinasiQuery = async (destination, origin) => {
  return new Promise((resolve, reject) => {
    let sql = `SELECT * FROM logistic where destination_name ilike '${destination}' and origin_name ilike '${origin}'`;

    db.query(sql)
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        console.log(err);
        return reject(err);
      });
  })
    .then((result) => {
      return result;
    })
    .catch((err) => {
      return err;
    });
};

module.exports = { getkurirQuery, kurirdestinasiQuery };

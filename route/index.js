const auth = require("./auth");
const logistic = require("./logistic");

module.exports = { auth, logistic };

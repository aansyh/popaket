require("dotenv").config();
const jwt = require("jsonwebtoken");
const { authQuery } = require("../query");
const CryptoJs = require("crypto-js");

const register = (req, res) => {
  try {
    let { username, password, msisdn } = req.body;

    password = CryptoJs.SHA1(password);

    msisdn = parseInt(`62${msisdn}`);

    let body = {
      username: username,
      password: password.toString(CryptoJs.enc.Base64),
      msisdn: msisdn,
    };

    if (req.body !== null) {
      if (!(username && password && msisdn)) {
        res.status(400).send({
          code: 400,
          message: "All input is required",
          data: {},
          err: "Bad Request",
        });
      } else {
        authQuery.registerQuery(body).then((resp) => {
          if (resp.rowCount >= 1) {
            res.status(409).send({
              code: 409,
              message: "There is duplicate with our database",
              data: {},
              err: "Conflict",
            });
          } else {
            const token = jwt.sign({ id: resp.id }, process.env.SECRET, {
              expiresIn: "2h",
            });

            let data = {
              token: token,
              username: resp.username,
            };

            res.status(201).send({
              code: 201,
              message: "Success register new user",
              data: data,
              err: null,
            });
          }
        });
      }
    } else {
      res.status(400).send({
        code: 400,
        message: "Missing body",
        data: {},
        err: "Bad Request",
      });
    }
  } catch (error) {
    console.log(error.stack);
    res.status(500).send({
      code: 500,
      message: "There an error in our server",
      data: {},
      err: "Internal Server Error",
    });
  }
};

const login = (req, res) => {
  try {
    let { password, msisdn } = req.body;

    password = CryptoJs.SHA1(password);

    msisdn = parseInt(`62${msisdn}`);

    let body = {
      password: password.toString(CryptoJs.enc.Base64),
      msisdn: msisdn,
    };

    if (req.body !== null) {
      if (!(password && msisdn)) {
        res.status(400).send({
          code: 400,
          message: "All input is required",
          data: {},
          err: "Bad Request",
        });
      }

      authQuery.loginQuery(body).then((resp) => {
        if (resp.rowCount >= 1) {
          const token = jwt.sign({ id: resp.rows }, process.env.SECRET, {
            expiresIn: "2h",
          });

          let data = {
            token: token,
          };

          res.status(200).send({
            code: 200,
            message: "Success get data person",
            data: data,
            err: null,
          });
        } else {
          res.status(404).send({
            code: 404,
            message: "Theres no data with that",
            data: {},
            err: "Not Found",
          });
        }
      });
    } else {
      res.status(400).send({
        code: 400,
        message: "Missing body",
        data: {},
        err: "Bad Request",
      });
    }
  } catch (error) {
    console.log(error.stack);
    res.status(500).send({
      code: 500,
      message: "There an error in our server",
      data: {},
      err: "Internal Server Error",
    });
  }
};

const getdata = (req, res) => {
  try {
    console.log(req.user.id[0]);
    if (req.user.id.length >= 1) {
      let data = req.user.id[0];

      return res.status(200).send({
        code: 200,
        message: "Success get data",
        data: data,
        err: null,
      });
    }
  } catch (error) {
    console.log(error.stack);
    res.status(500).send({
      code: 500,
      message: "There an error in our server",
      data: {},
      err: "Internal Server Error",
    });
  }
};

module.exports = { register, login, getdata };

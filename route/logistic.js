const { logisticQuery } = require("../query");

const getdatakurir = (req, res) => {
  try {
    logisticQuery.getkurirQuery().then((resp) => {
      let data = resp[0];

      return res.status(200).send({
        code: 200,
        message: "Success get data",
        data: data,
        err: null,
      });
    });
  } catch (error) {
    console.log(error.stack);
    res.status(500).send({
      code: 500,
      message: "There an error in our server",
      data: {},
      err: "Internal Server Error",
    });
  }
};

const kurirdestinasi = (req, res) => {
  try {
    let { destination_name, origin_name } = req.query;

    destination_name = destination_name.toUpperCase();
    origin_name = origin_name.toUpperCase();

    if (!(destination_name || origin_name)) {
      return res.status(400).send({
        code: 400,
        message: "All query is require",
        data: {},
        err: "Bad Request",
      });
    } else {
      logisticQuery
        .kurirdestinasiQuery(destination_name, origin_name)
        .then((resp) => {
          return res.status(200).send({
            code: 200,
            message: "Success get data",
            data: resp.rows,
            err: null,
          });
        });
    }
  } catch (error) {
    console.log(error.stack);
    res.status(500).send({
      code: 500,
      message: "There an error in our server",
      data: {},
      err: "Internal Server Error",
    });
  }
};
module.exports = { getdatakurir, kurirdestinasi };

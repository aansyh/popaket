require("dotenv").config();
const { Pool } = require("pg");

const envs = process.env;

let obj = {
  user: envs.PG_USER,
  host: envs.PG_HOST,
  database: envs.PG_DB,
  password: envs.PG_PASS,
  port: envs.PG_PORT,
};

const client = new Pool(obj);
module.exports = client;

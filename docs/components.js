module.exports = {
  components: {
    schemas: {
      Person: {
        type: "object",
        properties: {
          id: {
            type: "integer",
            description: "person id",
            example: "123",
          },
          username: {
            type: "string",
            description: "person name",
            example: "yuyun kamuyun",
          },
          msisdn: {
            type: "integer",
            description: "person identifier",
            example: "62123",
          },
          password: {
            type: "string",
            description: "person password encrypted SH1",
            example: "kabsdi1iwiua",
          },
        },
      },
      RegisterInput: {
        type: "object",
        properties: {
          username: {
            type: "string",
            description: "person name",
            example: "yuyun kamuyun",
          },
          msisdn: {
            type: "integer",
            description: "person identifier",
            example: "62123",
          },
          password: {
            type: "string",
            description: "person password encrypted SH1",
            example: "kabsdi1iwiua",
          },
        },
      },
      LoginInput: {
        type: "object",
        properties: {
          msisdn: {
            type: "integer",
            description: "person identifier",
            example: "85156",
          },
          password: {
            type: "string",
            description: "person password encrypted SH1",
            example: "newPass123",
          },
        },
      },
      Logistic: {
        type: "object",
        properties: {
          destination_name: {
            type: "string",
            description: "delivery destination",
            example: "SUBANG",
          },
          logistic_name: {
            type: "string",
            description: "delivery service name",
            example: "Iam Ninja",
          },
          origin_name: {
            type: "string",
            description: "origin of delivery",
            example: "GARUT",
          },
          amount: {
            type: "integer",
            description: "number of deliveries",
            example: 10000,
          },
          duration: {
            type: "string",
            description: "delivery time",
            example: "1",
          },
        },
      },
      LogisticInput: {
        type: "object",
        properties: {
          destination_name: {
            type: "string",
            description: "delivery destination",
            example: "SUBANG",
          },
          origin_name: {
            type: "string",
            description: "origin of delivery",
            example: "GARUT",
          },
        },
      },
      ApiResponse: {
        type: "object",
        properties: {
          code: {
            type: "integer",
            description: "status code http",
            example: 200,
          },
          message: {
            type: "string",
            description: "info to user",
            example: "success, get data",
          },
          data: {
            type: "object",
            description: "deliver data response",
          },
          err: {
            type: "string",
            description: "show status http",
            example: null,
          },
        },
      },
    },
  },
};

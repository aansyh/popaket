const basicInfo = require("./basicInfo");
const components = require("./components");
const server = require("./server");
const tags = require("./tags");
const auth = require("./auth");
const logistic = require("./logistic");

module.exports = {
  ...basicInfo,
  ...components,
  ...server,
  ...tags,
  ...auth,
  ...logistic,
};

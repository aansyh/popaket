const getPerson = require("./getPerson");
const login = require("./login");
const register = require("./register");

module.exports = {
  paths: {
    "/register": { ...register },
    "/login": { ...login },
    "/getperson": { ...getPerson },
  },
};

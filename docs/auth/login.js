module.exports = {
  post: {
    tags: ["Person API"],
    description: "Login Person",
    operationId: "loginPerson",
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/LoginInput",
          },
        },
      },
    },
    responses: {
      201: {
        description: "person created",
      },
      500: {
        description: "Server error",
      },
    },
  },
};

module.exports = {
  post: {
    tags: ["Person API"],
    description: "Register Person",
    operationId: "registerPerson",
    parameters: [],
    requestBody: {
      content: {
        "application/json": {
          schema: {
            $ref: "#/components/schemas/RegisterInput",
          },
        },
      },
    },
    responses: {
      201: {
        description: "person created ",
      },
      500: {
        description: "Server error",
      },
    },
  },
};

module.exports = {
  get: {
    tags: ["Logistic API"],
    description: "Get data kurir using token authorization",
    operationId: "getDataKurir",
    parameters: [
      {
        name: "destination_name",
        in: "query",
        schema: {
          $ref: "#/components/schemas/LogisticInput",
        },
        required: true,
        description: "destination_name",
      },
    ],
    requestBody: {},
    responses: {
      200: {
        description: "success, get data",
      },
      500: {
        description: "Server error",
      },
    },
  },
};

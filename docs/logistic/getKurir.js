module.exports = {
  get: {
    tags: ["Logistic API"],
    description: "Get data kurir using token authorization",
    operationId: "getKurir",
    parameters: [
      {
        name: "Authorization",
        in: "headers",
        required: true,
        style: "simple",
        schema: {
          type: "string",
          example:
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6W3siaWQiOjIwLCJ1c2VybmFtZSI6IkFhbiBTeWFoIiwibXNpc2RuIjo2Mjg1MTU2LCJwYXNzd29yZCI6InBFSVVYYlRlejZjVHJxOWhyVnFWWWJjQWF2WT0ifV0sImlhdCI6MTYzMjM3MTU1OCwiZXhwIjoxNjMyMzc4NzU4fQ.owuP5z0mGbz561p5q0jXKca4TIMTf8d3-l9UMiB7ujk",
        },
      },
    ],
    requestBody: {},
    responses: {
      200: {
        description: "success, get data",
      },
      500: {
        description: "Server error",
      },
    },
  },
};

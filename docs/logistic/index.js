const getKurir = require("./getKurir");
const kurir = require("./kurir");

module.exports = {
  paths: {
    "/getkurir": { ...getKurir },
    "/kurir": { ...kurir },
  },
};

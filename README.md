# Popaket

Test from PT Tradaru Digital Niaga

---

## Requirements

For development, you will only need Node.js and a node global package, Yarn, installed in your environement.

### Node

- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
  Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

## Install

    $ git clone https://gitlab.com/aansyh/popaket.git
    $ cd popaket
    $ npm install

## Configure app

Create file '.env' then edit it with your settings. You will need:

- Define PORT;
- Define PG_USER;
- Define PG_HOST;
- DEfine PG_DB;
- Define PG_PASS;
- Define PG_PORT;
- Define SECRET;

## Running the project

    $ npm run dev

    or

    $ nodemon
